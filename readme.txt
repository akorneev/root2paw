This is a tool for conversion of ROOT (.root) data format to
PAW (.hbook) data format.

Usage:
  
  Let's say you want to covert file test.root to file test.hbook:
  
  $ ssh lxplus.cern.ch
  $ svn co https://svn.cern.ch/reps/p348/paw
  $ cd paw
  $ root -b -q -l 'root2hbook.c("test.root")'
  $ /afs/cern.ch/sw/lcg/external/cernlib/2006a/x86_64-slc6-gcc44-opt/bin/paw -b test.kumac
  
  The output is in the file test.hbook


How to make a plot with PAW?

  Run PAW:
  $ /afs/cern.ch/sw/lcg/external/cernlib/2006a/x86_64-slc6-gcc44-opt/bin/paw
  
  > hi/file 1 test.hbook
  > hi/list
  > hi/plot 1
  > quit
  
  
Reference:

  1. the tool is based on:
  
    http://lhcb-reconstruction.web.cern.ch/lhcb-reconstruction/Software/root_to_hbook.htm

  2. PAW documentation:
  
    http://paw.web.cern.ch/paw/
